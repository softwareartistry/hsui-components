const path = require("path");

let config = {
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
    libraryTarget: "umd",
    library: "[name]"
  },
  module: {
    rules: [{
      test: /\.js/,
      use: {
        loader: "babel-loader",
        options: {
          presets: [
            ["env"],
            ["react"]
          ]
        }
      }
    }, {
      test: /\.css/,
      use: {
        loader: "css-loader"
      }
    }, {
      test: /\.(woff|woff2|ttf|eot|svg|ico|gif|png)(\?v=[a-z0-9]\.[a-z0-9]\.[a-z0-9])?$/,
      use: "url-loader"
    }]
  },
  externals: {
    react: {
      commonjs: "react",
      commonjs2: "react",
      amd: "React",
      root: "React"
    },
    "react-dom": {
      commonjs: "react-dom",
      commonjs2: "react-dom",
      amd: "react-dom",
      root: "react-dom"
    },
    "prop-types": {
      commonjs: "prop-types",
      commonjs2: "prop-types",
      amd: "prop-types",
      root: "prop-types"
    },
    "material-ui": {
      commonjs: "material-ui",
      commonjs2: "material-ui",
      amd: "material-ui",
      root: "material-ui"
    },
    "material-ui/DatePicker/dateUtils": {
      commonjs: "material-ui/DatePicker/dateUtils",
      commonjs2: "material-ui/DatePicker/dateUtils",
      amd: "material-ui/DatePicker/dateUtils",
      root: "material-ui/DatePicker/dateUtils"
    },
    "material-ui/DatePicker/DayButton": {
      commonjs: "material-ui/DatePicker/DayButton",
      commonjs2: "material-ui/DatePicker/DayButton",
      amd: "material-ui/DatePicker/DayButton",
      root: "material-ui/DatePicker/DayButton"
    },
    "material-ui/styles/colors": {
      commonjs: "material-ui/styles/colors",
      commonjs2: "material-ui/styles/colors",
      amd: "material-ui/styles/colors",
      root: "material-ui/styles/colors"
    },
    "material-ui/styles/transitions": {
      commonjs: "material-ui/styles/transitions",
      commonjs2: "material-ui/styles/transitions",
      amd: "material-ui/styles/transitions",
      root: "material-ui/styles/transitions"
    },
    "material-ui/internal/SlideIn": {
      commonjs: "material-ui/internal/SlideIn",
      commonjs2: "material-ui/internal/SlideIn",
      amd: "material-ui/internal/SlideIn",
      root: "material-ui/internal/SlideIn"
    },
    "material-ui/Popover/PopoverAnimationVertical": {
      commonjs: "material-ui/Popover/PopoverAnimationVertical",
      commonjs2: "material-ui/Popover/PopoverAnimationVertical",
      amd: "material-ui/Popover/PopoverAnimationVertical",
      root: "material-ui/Popover/PopoverAnimationVertical"
    },
    lodash: {
      commonjs: "lodash",
      commonjs2: "lodash",
      amd: "lodash",
      root: "lodash"
    },
    luxon: {
      commonjs: "luxon",
      commonjs2: "luxon",
      amd: "luxon",
      root: "luxon"
    },
    "lang-engine": {
      commonjs: "lang-engine",
      commonjs2: "lang-engine",
      amd: "lang-engine",
      root: "lang-engine"
    },
    "react-joyride": {
      commonjs: "react-joyride",
      commonjs2: "react-joyride",
      amd: "react-joyride",
      root: "react-joyride"
    },
    "react-joyride/lib/react-joyride-compiled.css": {
      commonjs: "react-joyride/lib/react-joyride-compiled.css",
      commonjs2: "react-joyride/lib/react-joyride-compiled.css",
      amd: "react-joyride/lib/react-joyride-compiled.css",
      root: "react-joyride/lib/react-joyride-compiled.css"
    }
  }
};

module.exports = config;
