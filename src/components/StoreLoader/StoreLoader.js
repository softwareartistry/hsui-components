import React, {Component} from "react";
import PropTypes from "prop-types";
import StoreActionsViewer from "../StoreActionsViewer/StoreActionsViewer";
import {Container} from "./StoreLoaderStyledComponent";

class StoreLoader extends Component {
  constructor(props, context) {
    super(props, context);
    this.store = window.reactBridge.stores[this.props.storeName];
    window.currentStore = this.store;
    this.actionList = this.store.getAllEvents();
    console.log("Events", this.actionList);
    console.log("Try dispatching some Events");
    console.log(`Ex - reactBridge.dispatcher.publish("${Object.keys(this.actionList)[0]}")`);

    this.state = {
      jsonData: this.store.getState()
    };

    this.unsub = this.store.onChange(() => {
      this.setState({
        jsonData: this.store.getState()
      });
    });
    console.log("%c StoreLoader Component -> Init ", "background: red; color: white");
  }

  componentWillUnmount() {
    this.unsub();
    console.log("%c StoreLoader Component -> UnMount ", "background: black; color: yellow");
  }

  render() {
    console.log("%c StoreLoader Component -> Render ", "background: black; color: pink");
    return (
      <Container>
        <h1>{this.props.storeName}</h1>
        {this.state.jsonData}
        <StoreActionsViewer actionList={this.actionList} dispatcher={this.props.dispatcher}></StoreActionsViewer>
      </Container>
    );
  }
}
StoreLoader.defaultProps = {
  storeName: null,
  dispatcher: null
};
StoreLoader.propTypes = {
  storeName: PropTypes.string.isRequired,
  dispatcher: PropTypes.object.isRequired
};
export default StoreLoader;
