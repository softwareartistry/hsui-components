import React, {Component} from "react";
import {Container} from "./FullScreenButtonStyledComponent";
import {FontIcon} from "material-ui";
import screenfull from "screenfull";
//TODO - stateless

class FullScreenButton extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle(event) {
    if (screenfull.enabled) {
      let node = event.currentTarget;
      while(node !== null) {
        if (node.dataset !== undefined && node.dataset.fsBoundary !== undefined) {
          break;
        }
        node = node.parentNode;
      }
      screenfull.toggle(node);
    }
  }

  render() {
    return (
      <Container className={this.props.className} onClick={this.handleToggle} data-isfs="false">
        <FontIcon className={`material-icons`} style={{color: "white", position: "cursor"}}>zoom_out_map</FontIcon>
      </Container>
    );
  }
}
export default FullScreenButton;
