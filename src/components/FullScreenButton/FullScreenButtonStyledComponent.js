import styled from "styled-components";

const Container = styled.div`
  position: relative;
  cursor: pointer;
  display: inline-block;
  padding-left: 3.5px;
  padding-top: 2px;
  padding-bottom: 2px;
  padding-right: 3.5px;
  border-radius: 3px;
  font-size: 18px;
`;

export {
  Container
};
