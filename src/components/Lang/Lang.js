import React, {Component} from "react";
import PropTypes from "prop-types";
import langEngine from "lang-engine";

class Lang extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    var str = "";
    if (this.props.namespace === "") {
      str = langEngine.resolve(this.props.langKey);
    } else {
      str = langEngine.resolve(`${this.props.namespace}_${this.props.langKey}`);
    }
    return (
      <span>{str}</span>
    );
  }
}

Lang.defaultProps = {
  namespace: ""
};
Lang.propTypes = {
  namespace: PropTypes.string,
  langKey: PropTypes.string.isRequired
};

export default Lang;
