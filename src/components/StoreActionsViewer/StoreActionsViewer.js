import React, {Component} from "react";
import PropTypes from "prop-types";
import ReactDOM from "react-dom";
import {forOwn} from "lodash";
import {Container, Listnone, LiStyle, InputStyleComponent} from "./StoreActionsViewerStyledComponent";

class StoreActionsViewer extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(action) {
    let arr = [action];
    this.props.actionList[action].forEach((arg) => {
      arr.push(ReactDOM.findDOMNode(this.refs[action +arg]).value);
    });
    this.props.dispatcher.publish.apply(this.props.dispatcher, arr);
  }

  render() {
    return (
      <Container>
        <h1>ActionList</h1>
        <Listnone>
          {
            forOwn(this.props.actionList, (args, action) => {
              return <LiStyle key={action}>
                <button onClick={()=>{
                  this.handleClick(action);
                }}>{action}</button>
                {
                  args.map((arg)=> {
                    return <InputStyleComponent key={action + arg} ref={action + arg} placeholder={arg}/>;
                  })
                }
              </LiStyle>;
            })
          }
        </Listnone>
      </Container>
    );
  }
}
StoreActionsViewer.defaultProps = {};
StoreActionsViewer.propTypes = {
  actionList: PropTypes.object,
  dispatcher: PropTypes.any
};
export default StoreActionsViewer;
