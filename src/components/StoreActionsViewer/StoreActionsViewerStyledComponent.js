import styled from "styled-components";
import {TextField} from "material-ui";

const Container = styled.div`
  position: relative;
`;

const Listnone = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
`;

const LiStyle = styled.li`
  padding: 2px;
`;

const InputStyleComponent = styled(TextField)`
  width: 142px;
  display: inline;
  marginLeft: 10px;
`;

export {
  Container,
  Listnone,
  LiStyle,
  InputStyleComponent
};
