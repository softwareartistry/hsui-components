import styled, {keyframes} from "styled-components";
import {IconButton} from "material-ui";

const spinclockwise = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const IconSpin = styled(IconButton)`
  animation: ${props => {
    return props.spin ? `${spinclockwise} 2000ms linear infinite;` : "";
  }}
`;

export {
  IconSpin
};
