import React from "react";
import PropTypes from "prop-types";
import {IconSpin} from "./IconStyledComponent";

let Icon = function (props) {
  return <IconSpin
    className={`${props.className}`}
    style={props.fast === true ? {animationDuration: "0.5s"} : ""}
    iconStyle={props.style}
    spin={props.spin}
    iconClassName="material-icons"
    tooltip={props.tooltip !== "" ? props.tooltip : ""}
    tooltipPosition={props.tooltipPlacement}
    onClick={props.onClick}>
    {props.iconName}
  </IconSpin>;
};

Icon.propTypes = {
  style: PropTypes.object,
  spin: PropTypes.bool,
  fast: PropTypes.bool,
  iconName: PropTypes.string.isRequired,
  tooltip: PropTypes.any,
  size: PropTypes.string,
  onClick: PropTypes.func,
  tooltipPlacement: PropTypes.string
};

Icon.defaultProps = {
  style: {},
  spin: false,
  fast: false,
  icon: "",
  tooltip: "",
  tooltipPlacement: "right",
  size: "normal",
  onClick: function () {
    //TODO
  }
};

export default Icon;
