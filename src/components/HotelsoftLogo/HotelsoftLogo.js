import React from "react";
import logo from "./hotelsoft_logo.svg";
import {Container, Image} from "./HotelsoftLogoStyledComponent";

const HotelsoftLogo = function (props) {
  return (
    <Container className={props.className}>
      <Image src={logo} alt="Hotelsoft"/>
    </Container>
  );
};
export default HotelsoftLogo;
