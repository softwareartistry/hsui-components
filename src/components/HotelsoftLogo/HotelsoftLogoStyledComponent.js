import styled from "styled-components";

const Container = styled.div`
  position: relative
`;

const Image = styled.img`
  position: relative;
  width: 75% !important;
  top: -1px;
`;

export {
  Container,
  Image
};
