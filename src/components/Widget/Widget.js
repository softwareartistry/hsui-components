import React from "react";
import PropTypes from "prop-types";
// import Icon from "./../Icon/Icon";
import FullScreenButton from "./../FullScreenButton/FullScreenButton";
import {
  Container,
  Header,
  HeaderRightSection,
  HeaderLeftSection,
  IconLeft,
  IconRight,
  Title
} from "./WidgetStyledComponent";

let Widget = function (props) {
  return (
    <Container>
      <Header>
        <HeaderLeftSection>
          <IconLeft iconName={props.icon}/>
          <Title>{props.title}</Title>
        </HeaderLeftSection>
        <HeaderRightSection>
          {
            props.showInfoIcon ? <IconRight iconName="info" tooltipPlacement="top-left" tooltip={props.infoTooltip}/> : ""
          }
          {
            props.showFullScreenButton ? <FullScreenButton/> : ""
          }
        </HeaderRightSection>
      </Header>
      <div>
        {props.children}
      </div>
    </Container>
  );
};
Widget.defaultProps = {
  icon: null,
  title: null,
  showFullScreenButton: false,
  showInfoIcon: false,
  infoTooltip: ""
};
Widget.propTypes = {
  icon: PropTypes.any.isRequired,
  title: PropTypes.string.isRequired,
  showFullScreenButton: PropTypes.bool,
  style: PropTypes.object,
  showInfoIcon: PropTypes.bool,
  infoTooltip: PropTypes.string
};
export default Widget;
