import styled from "styled-components";
import Icon from "./../Icon/Icon";

const Container = styled.div`
  position: relative;
  border: 1px solid #D3D3D3;
  color: #333;
`;

const Header = styled.div`
  background: #FAFAFA;
  box-sizing: border-box;
  font-size: 14px;
  padding-left: 7px;
  border-bottom: 1px solid #D3D3D3;
  display: inline-flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  &::after: {
    visibility: hidden;
    display: block;
    font-size: 0;
    content: " ";
    clear: both;
    height: 0;
  }
`;

const Title = styled.span`
  color: #646464;
}
`;

const IconLeft = styled(Icon)`
  margin-right: 5px;
  padding: 0 !important;
  width: auto !important;
  height: auto !important;
`;

const IconRight = styled(Icon)`
  padding: 0 !important;
  width: auto !important;
  height: auto !important;
`;

const HeaderLeftSection = styled.div`
  composes: headerCommon;
  display: flex;
  float: left;
`;

const HeaderRightSection = styled.div`
  composes: headerCommon;
  border-left: 1px solid #D3D3D3;
  padding-left: 4px;
  padding-right: 3px;
  float: right;
`;

export {
  Container,
  Header,
  HeaderRightSection,
  HeaderLeftSection,
  IconLeft,
  Title,
  IconRight
};
