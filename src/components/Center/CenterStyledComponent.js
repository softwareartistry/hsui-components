import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

const ContainerColumn = styled.div`
  composes: container;
  flex-direction: column;
`;

export {
  Container,
  ContainerColumn
};
