import React from "react";
import PropTypes from "prop-types";
import {Container, ContainerColumn} from "./CenterStyledComponent";

var Center = function (props) {
  if (props.column === true) {
    return (<ContainerColumn>
      {props.children}
    </ContainerColumn>);
  } else {
    return (<Container>
      {props.children}
    </Container>);
  }
};

Center.defaultProps = {
  column: false
};
Center.propTypes = {
  column: PropTypes.bool
};
export default Center;
