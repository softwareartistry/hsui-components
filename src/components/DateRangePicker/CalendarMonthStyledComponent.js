import styled from "styled-components";

const RootContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    font-weight: 400;
    height: 228;
    line-height: 2;
    position: relative;
    text-align: center;
    -moz-padding-start: 0;
`;

const Week = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    height: 34px;
    margin-bottom: 2px;
`;

export {
  RootContainer,
  Week
};
