import React, {Component} from "react";
import PropTypes from "prop-types";
import {dateTimeFormat, addMonths, isAfterDate} from "material-ui/DatePicker/dateUtils";
import {TextField} from "material-ui";
import {DateTime} from "luxon";
import DateRangeSelectorDialog from "./DateRangeSelectorDialog";

const millisecondsMultiplier = 60000, DB_DATE_FORMAT = "yyyy-MM-dd";

function fDate(date) {
  let dateTimestampOfClientMidnight = new Date(date).getTime() + (new Date(date).getTimezoneOffset() * millisecondsMultiplier);
  return date ? new Date(dateTimestampOfClientMidnight) : date;
}

class DateRangePicker extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      rangeStart: fDate(this.props.value[0]),
      rangeEnd: fDate(this.props.value[1]),
      pickerOpen: false,
      firstMonthDay: fDate(this.props.firstMonthDay),
      lastRangeStart: fDate(this.props.value[0]),
      lastRangeEnd: fDate(this.props.value[1])
    };

    this.handleAccept = this.handleAccept.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleChangeRange = this.handleChangeRange.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleTouchTap = this.handleTouchTap.bind(this);
    this.handlePrevMonthClicked = this.handlePrevMonthClicked.bind(this);
    this.handleNextMonthClicked = this.handleNextMonthClicked.bind(this);
    this.handleSetRange = this.handleSetRange.bind(this);
  }

  get value() {
    return {
      start: fDate(this.state.rangeStart),
      end: fDate(this.state.rangeEnd)
    };
  }

  getRangeLabelValue(rangeStart, rangeEnd) {
    let defaultRangeDateFormat = "ccc, MMM dd, yyyy";
    if (rangeStart === null) {
      return "";
    } else {
      const rangeStartObj = rangeStart ? DateTime.fromObject({
        day: rangeStart.getDate(),
        month: rangeStart.getMonth() + 1,
        year: rangeStart.getFullYear()
      }) : DateTime.local(),
      rangeEndObj = rangeEnd ? DateTime.fromObject({
        day: rangeEnd.getDate(),
        month: rangeEnd.getMonth() + 1,
        year: rangeEnd.getFullYear()
      }) : DateTime.local();
      if (this.props.rangeLabelDateFormat) {
        return `${rangeStartObj.toFormat(this.props.rangeLabelDateFormat)} to ${rangeEndObj.toFormat(this.props.rangeLabelDateFormat)}`;
      } else {
        return `${rangeStartObj.toFormat(defaultRangeDateFormat)} to ${rangeEndObj.toFormat(defaultRangeDateFormat)}`;
      }
    }
  }

  sanityChangeRangeStartRangeEnd(rangeStart, rangeEnd) {
    if (rangeStart === null && rangeEnd !== null) {
      return {
        rangeStart: rangeEnd,
        rangeEnd: rangeEnd
      };
    } else {
      return {
        rangeStart: rangeStart,
        rangeEnd: rangeEnd
      };
    }
  }

  componentWillReceiveProps(nextProps) {
    if ((nextProps.value[0] !== this.props.value[0])
      || (nextProps.value[1] !== this.props.value[1])
      || (nextProps.firstMonthDay !== this.props.firstMonthDay)) {
      this.setState({
        rangeStart: fDate(nextProps.value[0]),
        rangeEnd: fDate(nextProps.value[1]),
        firstMonthDay: fDate(nextProps.firstMonthDay),
        lastRangeStart: fDate(nextProps.value[0]),
        lastRangeEnd: fDate(nextProps.value[1]),
      });
    }
  }

  handleAccept() {
    let selectedRangeStart = this.state.rangeStart;
    let selectedRangeEnd = this.state.rangeEnd;
    if (typeof this.props.onAccept === "function") {
      const selectedRangeStartObj = selectedRangeStart ? DateTime.fromObject({
        day: selectedRangeStart.getDate(),
        month: selectedRangeStart.getMonth() + 1,
        year: selectedRangeStart.getFullYear()
      }) : DateTime.local(),
      selectedRangeEndObj = selectedRangeEnd ? DateTime.fromObject({
        day: selectedRangeEnd.getDate(),
        month: selectedRangeEnd.getMonth() + 1,
        year: selectedRangeEnd.getFullYear()
      }) : DateTime.local();
      this.props.onAccept([selectedRangeStartObj.toFormat(DB_DATE_FORMAT), selectedRangeEndObj.toFormat(DB_DATE_FORMAT)]);
    }
    let nrs = this.sanityChangeRangeStartRangeEnd(selectedRangeStart, selectedRangeEnd).rangeStart;
    let nre = this.sanityChangeRangeStartRangeEnd(selectedRangeStart, selectedRangeEnd).rangeEnd;
    this.setState({
      pickerOpen: false,
      firstMonthDay: nrs === null ? fDate(this.props.firstMonthDay): nrs,
      lastRangeStart: nrs,
      lastRangeEnd: nre
    });
  }

  handleCancel() {
    if (typeof this.props.onCancel === "function") {
      this.props.onCancel();
    }
    let nrs = this.sanityChangeRangeStartRangeEnd(this.state.lastRangeStart, this.state.lastRangeEnd).rangeStart;
    let nre = this.sanityChangeRangeStartRangeEnd(this.state.lastRangeStart, this.state.lastRangeEnd).rangeEnd;
    this.setState({
      pickerOpen: false,
      firstMonthDay: nrs === null? fDate(this.props.firstMonthDay): nrs,
      rangeStart: nrs,
      rangeEnd: nre
    });
  }

  handleReset() {
    if (typeof this.props.onReset === "function") {
      this.props.onReset();
    }
    let nrs = this.sanityChangeRangeStartRangeEnd(fDate(this.props.value[0]), this.props.value[1]).rangeStart;
    let nre = this.sanityChangeRangeStartRangeEnd(fDate(this.props.value[0]), this.props.value[1]).rangeEnd;
    this.setState({
      rangeStart: nrs,
      rangeEnd: nre
    });
  }

  handleChangeRange(date) {
    let nrs = date, nre = date;
    if (this.state.rangeStart !== null &&
      (this.state.rangeEnd === null || (this.state.rangeStart === this.state.rangeEnd))) {
      nrs = this.state.rangeStart;
    }

    if (isAfterDate(nrs, nre)) {
      nrs = nre;
    }
    nrs = this.sanityChangeRangeStartRangeEnd(nrs, nre).rangeStart;
    nre = this.sanityChangeRangeStartRangeEnd(nrs, nre).rangeEnd;
    this.setState({
      rangeStart: nrs,
      rangeEnd: nre
    });
    if (typeof this.props.onChangeDateRange === "function") {
      this.props.onChangeDateRange(nrs, nre);
    }
  }

  handleSetRange(rangeStart, rangeEnd) {
    let newRangeStart = rangeStart,
      newRangeEnd = rangeEnd;
    newRangeStart = this.sanityChangeRangeStartRangeEnd(newRangeStart, newRangeEnd).rangeStart;
    newRangeEnd = this.sanityChangeRangeStartRangeEnd(newRangeStart, newRangeEnd).rangeEnd;
    let setStateObject = {
      rangeStart: newRangeStart,
      rangeEnd: newRangeEnd
    };
    if (this.props.autoClose === true) {
      setStateObject.pickerOpen = false;
      setStateObject.firstMonthDay = newRangeStart === null? fDate(this.props.firstMonthDay): newRangeStart;
      if (typeof this.props.onAccept === "function") {
        const newRangeStartObj = newRangeStart ? DateTime.fromObject({
          day: newRangeStart.getDate(),
          month: newRangeStart.getMonth() + 1,
          year: newRangeStart.getFullYear()
        }) : DateTime.local(),
        newRangeEndObj = newRangeEnd ? DateTime.fromObject({
          day: newRangeEnd.getDate(),
          month: newRangeEnd.getMonth() + 1,
          year: newRangeEnd.getFullYear()
        }) : DateTime.local();
        this.props.onAccept([newRangeStartObj.toFormat(DB_DATE_FORMAT), newRangeEndObj.toFormat(DB_DATE_FORMAT)]);
      }
      this.setState(setStateObject);
    } else {
      if (typeof this.props.onChangeDateRange === "function") {
        this.props.onChangeDateRange(newRangeStart, newRangeEnd);
      }
      this.setState(setStateObject);
    }
  }

  handleFocus(event) {
    event.target.blur();
    if (this.props.onFocus) {
      this.props.onFocus(event);
    }
  }

  handleTouchTap(event) {
    if (!this.props.disabled) {
      let nrs = this.sanityChangeRangeStartRangeEnd(this.state.lastRangeStart, this.state.lastRangeEnd).rangeStart;
      let nre = this.sanityChangeRangeStartRangeEnd(this.state.lastRangeStart, this.state.lastRangeEnd).rangeEnd;
      this.setState({
        pickerOpen: true,
        firstMonthDay: nrs === null? fDate(this.props.firstMonthDay): nrs,
        rangeStart: nrs,
        rangeEnd: nre
      });
    }

    if (typeof this.props.onOpen === "function") {
      this.props.onOpen(event);
    }
  }

  handlePrevMonthClicked() {
    if (typeof this.props.onMonthChange === "function") {
      this.props.onMonthChange(addMonths(this.state.firstMonthDay, 1));
    }
    this.setState({
      firstMonthDay: addMonths(this.state.firstMonthDay, 1)
    });
  }

  handleNextMonthClicked() {
    if (typeof this.props.onMonthChange === "function") {
      this.props.onMonthChange(addMonths(this.state.firstMonthDay, -1));
    }
    this.setState({
      firstMonthDay: addMonths(this.state.firstMonthDay, -1)
    });
  }

  formatRangeDates(ranges) {
    if (ranges && ranges.length > 0) {
      let ranges1 = JSON.parse(JSON.stringify(ranges));
      ranges1.forEach((v) => {
        v.range.forEach((v1, i) => {
          v.range[i] = fDate(v1);
        });
      });
      return ranges1;
    }
  }

  render() {
    return (
      <div>
        <TextField id="e2et_selected_date_range"
          value={this.getRangeLabelValue(this.state.rangeStart, this.state.rangeEnd)}
          floatingLabelText={this.props.labelText}
          onFocus={this.handleFocus}
          onClick={this.handleTouchTap}
          floatingLabelStyle={{fontWeight: "100"}}
          style={{width: "320px"}}
          errorText={this.props.errorText}
        />
        <DateRangeSelectorDialog
          showDialog={this.state.pickerOpen}
          onChange={this.handleChangeRange}
          customRanges={this.formatRangeDates(this.props.customRanges)}
          onAccept={this.handleAccept}
          onCancel={this.handleCancel}
          onReset={this.handleReset}
          okLabel={this.props.okLabel}
          cancelLabel={this.props.cancelLabel}
          resetLabel={this.props.resetLabel}
          DateTimeFormat={dateTimeFormat}
          animation={this.props.animation}
          rangeStart={this.state.rangeStart}
          rangeEnd={this.state.rangeEnd}
          minDate={fDate(this.props.minDate)}
          maxDate={fDate(this.props.maxDate)}
          noOfCalendars={this.props.noOfCalendars}
          firstDayOfWeek={this.props.firstDayOfWeek}
          locale={this.props.locale}
          style={this.props.dialogStyles}
          onPrevMonthButtonClicked={this.handlePrevMonthClicked}
          onNextMonthButtonClicked={this.handleNextMonthClicked}
          firstMonthDay={this.state.firstMonthDay}
          setRange={this.handleSetRange}
          shouldDisableDate={this.props.shouldDisableDate}
        />
      </div>
    );
  }
}
DateRangePicker.defaultProps = {
  okLabel: "Ok",
  cancelLabel: "Cancel",
  locale: "en-US",
  labelText: "Select Date Range",
  value: [null, null],
  firstDayOfWeek: 1,
  autoClose: false,
  firstMonthDay: new Date(),
  noOfCalendars: 2,
  errorText: null
};
DateRangePicker.propTypes = {
  noOfCalendars: PropTypes.number,
  firstDayOfWeek: PropTypes.number,
  autoClose: PropTypes.bool,
  disabled: PropTypes.bool,
  labelText: PropTypes.string,
  okLabel: PropTypes.string,
  cancelLabel: PropTypes.string,
  resetLabel: PropTypes.string,
  locale: PropTypes.string,
  rangeLabelDateFormat: PropTypes.string,
  onOpen: PropTypes.func,
  onAccept: PropTypes.func,
  onCancel: PropTypes.func,
  onReset: PropTypes.func,
  onChangeDateRange: PropTypes.func,
  onFocus: PropTypes.func,
  onMonthChange: PropTypes.func,
  animation: PropTypes.func,
  value: PropTypes.array,
  customRanges: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    range: PropTypes.array
  })),
  minDate: PropTypes.any,
  maxDate: PropTypes.any,
  firstMonthDay: PropTypes.any,
  containerStyle: PropTypes.object,
  dialogStyles: PropTypes.object,
  shouldDisableDate: PropTypes.func,
  errorText: PropTypes.string
};

export default DateRangePicker;
