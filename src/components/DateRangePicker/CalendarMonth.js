//this tweaked calendarmonth will check for startDate and endDate in the enviornment. It will allow
//highlighting of date button if it is either startDate or endDate and light-highlighting of datebutton
//if it is inbetween startDate and endDate
//this startDate and endDate will come from props - and both can be either null or have a date value

import React, {Component} from "react";
import PropTypes from "prop-types";
import {isBetweenDates, isEqualDate, getWeekArray} from "material-ui/DatePicker/dateUtils";
import DayButton from "material-ui/DatePicker/DayButton";
import {pink200} from "material-ui/styles/colors";
import {RootContainer, Week} from "./CalendarMonthStyledComponent";

class CalendarMonth extends Component {
    constructor(props, context) {
        super(props, context);
        this.handleTouchTapDay = this.handleTouchTapDay.bind(this);
        this.shouldDisableDate = this.shouldDisableDate.bind(this);
        this.getWeekElements = this.getWeekElements.bind(this);
        this.getDayElements = this.getDayElements.bind(this);
    }

    handleTouchTapDay(event, date) {
        if (typeof this.props.onTouchTapDay === "function") {
            this.props.onTouchTapDay(event, date);
        }
    }

    shouldDisableDate(day) {
        if (day === null) {
            return false;
        }
        let disabled = !isBetweenDates(day, this.props.minDate, this.props.maxDate);
        if (!disabled && typeof this.props.shouldDisableDate === "function") {
            disabled = this.props.shouldDisableDate(day);
        }
        return disabled;
    }

    getWeekElements() {
        const weekArray = getWeekArray(this.props.displayDate, this.props.firstDayOfWeek);

        return weekArray.map((week, i) => {
            return (
                <Week key={i}>
                    {this.getDayElements(week, i)}
                </Week>
            );
        }, this);
    }

    getDayElements(week, i) {
        return week.map((day, j) => {
            const isRangeStartDate = isEqualDate(this.props.rangeStart, day);
            const isRangeEndDate = isEqualDate(this.props.rangeEnd, day);
            const disabled = this.shouldDisableDate(day);
            const selected = !disabled && (isRangeStartDate || isRangeEndDate);
            if (day) {
                let buttonBackgroundStyle = { display: "inline-flex"};
                if (this.props.rangeStart !== null && this.props.rangeEnd !== null && isBetweenDates(day, this.props.rangeStart, this.props.rangeEnd)) {
                    buttonBackgroundStyle = {
                      background: pink200,
                      display: "inline-flex"
                    };
                }
                return (
                    <div className={`dbd-${day.getMonth()}-${day.getDate()}`} key={`dbd${(i + j)}`}
                         style={buttonBackgroundStyle}>
                        <DayButton
                            DateTimeFormat={this.props.DateTimeFormat}
                            locale={this.props.locale}
                            date={day}
                            disabled={disabled}
                            key={`db${(i + j)}`}
                            onClick={this.handleTouchTapDay}
                            selected={selected}
                        />
                    </div>
                );
            } else {
                return (
                    <DayButton
                        DateTimeFormat={this.props.DateTimeFormat}
                        locale={this.props.locale}
                        date={day}
                        disabled={disabled}
                        key={`db${(i + j)}`}
                        onClick={this.handleTouchTapDay}
                        selected={selected}
                    />
                );
            }

        }, this);
    }

    render() {
        return (
            <RootContainer>
                {this.getWeekElements()}
            </RootContainer>
        );
    }
}

CalendarMonth.defaultProps = {};
CalendarMonth.propTypes = {
    DateTimeFormat: PropTypes.func.isRequired,
    displayDate: PropTypes.object.isRequired,
    firstDayOfWeek: PropTypes.number,
    locale: PropTypes.string.isRequired,
    maxDate: PropTypes.object,
    minDate: PropTypes.object,
    onTouchTapDay: PropTypes.func,
    rangeStart: PropTypes.object.isRequired,
    rangeEnd: PropTypes.object.isRequired,
    shouldDisableDate: PropTypes.func
};

export default CalendarMonth;
