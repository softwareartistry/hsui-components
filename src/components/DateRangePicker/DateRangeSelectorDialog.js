import React, {Component} from "react";
import PropTypes from "prop-types";
import EventListener from "react-event-listener";
import keycode from "keycode";
import PopoverAnimationVertical from "material-ui/Popover/PopoverAnimationVertical";
import DateRangeSelector from "./DateRangeSelector";
import {RaisedButton, FlatButton, Popover} from "material-ui";
import {DateTime} from "luxon";
import {pink200} from "material-ui/styles/colors";
import ScopedCSS from "./DateRangeSelectorDialogStyledComponent";

const popOverStyles = {
  dialogContentClass: {
    width: "310px",
    top: "72px",
    left: "237.109px"
  },
  dialogBodyContentClass: {
    padding: "0",
    minHeight: "300px",
    minWidth: "310px",
    left: "11vw"
  },
  dialogBodyStyle: {
    padding: 0,
    minHeight: "300px",
    minWidth: "310px",
    left: "11vw"
  }
};

//todo (vishal) make this stateless
class DateRangeSelectorDialog extends Component {
  constructor(props, context) {
    super(props, context);
    this.dismiss = this.dismiss.bind(this);
    this.handleAccept = this.handleAccept.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.setRange = this.setRange.bind(this);
    this.handleWindowKeyUp = this.handleWindowKeyUp.bind(this);
  }

  dismiss() {
    if (typeof this.props.onCancel === "function" && this.props.showDialog) {
      this.props.onCancel();
    }
  }

  handleWindowKeyUp(event) {
    switch (keycode(event)) {
      case "enter":
        this.handleAccept();
        break;
    }
  }

  handleAccept() {
    if (typeof this.props.onAccept === "function") {
      this.props.onAccept();
    }
  }

  handleCancel() {
    if (typeof this.props.onCancel === "function") {
      this.props.onCancel();
    }
  }

  setRange(rs, re) {
    if (typeof this.props.setRange === "function") {
      this.props.setRange(rs, re);
    }
  }

  //todo (vishal) use calendar action button instead
  render() {
    let {rangeStart, rangeEnd} = this.props;
    let selectedRange;
    return (
      <div ref="root" style={{position: "relative"}}>
        <Popover
          anchorEl={this.refs.root} // For Popover
          animation={this.props.animation || PopoverAnimationVertical} // For Popover
          bodyStyle={popOverStyles.dialogBodyContentClass}
          contentStyle={popOverStyles.dialogContentClass}
          ref="dialog"
          repositionOnUpdate={true}
          open={this.props.showDialog}
          onRequestClose={this.dismiss}
          style={popOverStyles.dialogBodyStyle}
        >
          <EventListener
            target="window"
            onKeyUp={this.handleWindowKeyUp}
          />
          <ScopedCSS>
            <div className="mainContainer">
              <div className="customRangeContainer">
                <div className="flexInnerContainer">
                  <DateRangeSelector
                    DateTimeFormat={this.props.DateTimeFormat}
                    locale={this.props.locale}
                    onTouchTap={(date)=> {
                      this.props.onChange(date);
                    }}
                    rangeStart={this.props.rangeStart}
                    rangeEnd={this.props.rangeEnd}
                    minDate={this.props.minDate}
                    maxDate={this.props.maxDate}
                    noOfCalendars={this.props.noOfCalendars}
                    firstMonthDay={this.props.firstMonthDay}
                    nextMonthButtonClicked={this.props.onPrevMonthButtonClicked}
                    prevMonthButtonClicked={this.props.onNextMonthButtonClicked}
                    shouldDisableDate={this.props.shouldDisableDate}/>
                </div>
                {
                  (this.props.customRanges !== undefined && this.props.customRanges.length > 0) ?
                    <div className="rangeButton">
                      <span className="customrange">Custom Range</span>
                      <div className="dateRangeContainer">
                        {
                          this.props.customRanges.map((v, i)=> {
                            const customRangeStart = v.range[0], customRangeEnd = v.range[1];

                            selectedRange = (customRangeStart ? DateTime.fromObject({
                                day: customRangeStart.getDate(),
                                month: customRangeStart.getMonth() + 1,
                                year: customRangeStart.getFullYear()
                            }) : DateTime.local()).startOf("day").equals((rangeStart ? DateTime.fromObject({
                                day: rangeStart.getDate(),
                                month: rangeStart.getMonth() + 1,
                                year: rangeStart.getFullYear()
                            }) : DateTime.local()).startOf("day")) && (customRangeEnd ? DateTime.fromObject({
                                day: customRangeEnd.getDate(),
                                month: customRangeEnd.getMonth() + 1,
                                year: customRangeEnd.getFullYear()
                            }) : DateTime.local()).startOf("day").equals((rangeEnd ? DateTime.fromObject({
                                day: rangeEnd.getDate(),
                                month: rangeEnd.getMonth() + 1,
                                year: rangeEnd.getFullYear()
                            }) : DateTime.local()).startOf("day"));
                            return <div key={`fbs${i}`} className="paddSpace">
                              <FlatButton
                                fullWidth={true}
                                label={v.label}
                                style={selectedRange ? {backgroundColor: pink200} : ""}
                                onClick={()=> {
                                  this.setRange(v.range[0], v.range[1]);

                                }}
                              />
                            </div>;
                          })
                        }</div>
                    </div>
                    : null
                }
              </div>
              <div className="okButton">
                <div>
                  <FlatButton
                    secondary={true}
                    label={this.props.cancelLabel} onClick={this.handleCancel}/>
                </div>
                <div>
                  <FlatButton
                    id="e2et_daterange_okbutton"
                    primary={true}
                    label={this.props.okLabel} onClick={this.handleAccept}/>
                </div>
                {
                  this.props.resetLabel? <div>
                    <RaisedButton label={this.props.resetLabel} onClick={this.props.onReset} primary={true}/>
                  </div>: ""
                }
              </div>
            </div>
          </ScopedCSS>
        </Popover>
      </div>
    );
  }
}

DateRangeSelectorDialog.defaultProps = {};
DateRangeSelectorDialog.propTypes = {
  showDialog: PropTypes.bool,
  onChange: PropTypes.func,
  customRanges: PropTypes.array,
  onAccept: PropTypes.func,
  onCancel: PropTypes.func,
  onReset: PropTypes.func,
  okLabel: PropTypes.string,
  cancelLabel: PropTypes.string,
  resetLabel: PropTypes.string,
  DateTimeFormat: PropTypes.func,
  animation: PropTypes.func,
  noOfCalendars: PropTypes.number,
  rangeStart: PropTypes.object,
  rangeEnd: PropTypes.object,
  minDate: PropTypes.object,
  maxDate: PropTypes.object,
  containerStyle: PropTypes.object,
  firstDayOfWeek: PropTypes.number,
  locale: PropTypes.string,
  style: PropTypes.object,
  onPrevMonthButtonClicked: PropTypes.func.isRequired,
  onNextMonthButtonClicked: PropTypes.func.isRequired,
  firstMonthDay: PropTypes.object.isRequired,
  setRange: PropTypes.func,
  shouldDisableDate: PropTypes.func
};

DateRangeSelectorDialog.contextTypes = {
  muiTheme: PropTypes.object.isRequired
};
export default DateRangeSelectorDialog;
