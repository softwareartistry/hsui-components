import styled from "styled-components";

const Container = styled.div`
    position: relative;
    display: inline-flex;
    justify-content: flex-start;
    flex-wrap: wrap;
    align-items: center;
    overflow: auto;
    width: 100%;
    height: 100%;
    &::-webkit-scrollbar {
        display: none;
    }
`;

const FlexContainer = styled.div`
    display: inline-flex;
    justify-content: center;
    align-self: center;
`;

export {
  Container,
  FlexContainer
};
