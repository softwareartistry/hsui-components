import React, {Component} from "react";
import PropTypes from "prop-types";
import RangeCalendar from "./RangeCalendar";
import {addMonths} from "material-ui/DatePicker/dateUtils";
import {Container, FlexContainer} from "./DateRangeSelectorStyledComponent";

class DateRangeSelector extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleTouhTapDay = this.handleTouhTapDay.bind(this);
    this.getMonth = this.getMonth.bind(this);
  }

  handleTouhTapDay(event, date){
    //todo
    if (typeof this.props.onTouchTap === "function"){
      this.props.onTouchTap(date);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.rangeStart !== this.props.rangeStart || nextProps.rangeEnd !== this.props.rangeEnd){
      this.setState({
        rangeStart: nextProps.rangeStart,
        rangeEnd: nextProps.rangeEnd
      });
    }
  }

  getMonth(index){
    var firstDayOftheMonth = new Date(this.props.firstMonthDay.getFullYear(), this.props.firstMonthDay.getMonth(), 1);
    return addMonths(firstDayOftheMonth, index);
  }

  render() {
    return (
      <Container>
        {
          Array(this.props.noOfCalendars).fill("").map((v, i)=>{
            return <FlexContainer key={i}>
              <RangeCalendar firstDayOfWeek={this.props.firstDayOfWeek}
                DateTimeFormat={this.props.DateTimeFormat}
                locale={this.props.locale}
                rangeStart={this.props.rangeStart}
                rangeEnd={this.props.rangeEnd}
                leftmost={i === 0}
                rightmost={i === (this.props.noOfCalendars-1)}
                month={this.getMonth(i)}
                nextMonthHandler={this.props.nextMonthButtonClicked}
                prevMonthHandler={this.props.prevMonthButtonClicked}
                maxDate={this.props.maxDate}
                minDate={this.props.minDate}
                shouldDisableDate={this.props.shouldDisableDate}
                onTouchTapDay={(event, date)=>{
                  this.handleTouhTapDay(event, date);
                }}
              />
            </FlexContainer>;
          })
        }
      </Container>
    );
  }
}
DateRangeSelector.defaultProps = {
  firstDayOfWeek: 1
};
DateRangeSelector.propTypes = {
  DateTimeFormat: PropTypes.func.isRequired,
  locale: PropTypes.string.isRequired,
  rangeStart: PropTypes.object.isRequired,
  rangeEnd: PropTypes.object.isRequired,
  onTouchTap: PropTypes.func,
  firstDayOfWeek: PropTypes.number,
  nextMonthButtonClicked: PropTypes.func.isRequired,
  prevMonthButtonClicked: PropTypes.func.isRequired,
  noOfCalendars: PropTypes.number,
  firstMonthDay: PropTypes.object.isRequired,
  maxDate: PropTypes.object,
  minDate: PropTypes.object,
  shouldDisableDate: PropTypes.func
};

export default DateRangeSelector;
