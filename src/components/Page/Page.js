import React, {Component} from "react";
import PropTypes from "prop-types";
import HelperButton from "../HelperButton/HelperButton";
import {FontIcon} from "material-ui";
import Joyride from "react-joyride";
import "react-joyride/lib/react-joyride-compiled.css";
import ScopedCSS from "./PageStyledComponent";
/*eslint-disable*/
class Page extends Component {
  constructor(props, context){
    super(props, context);
    this.state={
      showtour: false
    };
    this.handleHelperButtonClick = this.handleHelperButtonClick.bind(this);
  }
  handleHelperButtonClick(){
    this.setState({
      showtour: true
    });
    if (this.refs.joyride){
      this.refs.joyride.reset(true);
    }
    if (this.props.onHelperButtonClicked){
      this.props.onHelperButtonClicked();
    }
  }
  render(){
    return (
    <ScopedCSS>
      <div id="tourparent" className={[this.props.className, "container"]}>
        {
          this.props.tourConfig !== null && this.props.tourConfig !== "" ? <div className="tutorContainer">
              <Joyride
                ref="joyride"
                offsetParentSelector="#tourparent"
                run={(this.state.showtour)}
                {...this.props.tourConfig}
              />
            </div> : ""
        }
        <div className="pageWrapper">
          <div className="header">
            <div className={this.props.spydrContentView === false ? "rzrFontPosition" : "materialFontPosition"}>
              <FontIcon className="material-icons" style={{fontSize: "30"}}>
                {this.props.icon}
              </FontIcon>
            </div>
            <div className={"title"}>{this.props.title}</div>
            <div className={(this.props.tourConfig !== undefined && this.props.tourConfig !== null) ? "relative" : "none"}>
              <HelperButton onClick={this.handleHelperButtonClick} />
            </div>
          </div>
          <div className={this.props.subHeading ? "fontcontainer" : "none" }>
            <div className={this.props.spydrContentView === false ? "fonting" : "fonting1" } >
              {this.props.subHeading}
            </div>
          </div>
          <div className={this.props.spydrContentView === false ? "rzrpageContent" : "pageContent"}>
            {this.props.children}
          </div>
        </div>
        <div className="footer">
          Hotelsoft Inc © 2018
        </div>
      </div>
    </ScopedCSS>
    );
  }
}

var child = <div>
  <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
  <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
  <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
  <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
  <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
  <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>
</div>;
Page.defaultProps = {
  breadCrumbItems: "THIS/IS/THE/DEFAULT/BREADCRUMB",
  icon: "send",
  title: "DEFAULT TITLE",
  subHeading: " ",
  children: child,
  tourConfig: null,
  spydrContentView: false
};

Page.propTypes = {
  breadCrumbItems: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.string]).isRequired,
  icon: PropTypes.string.isRequired,
  subHeading: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.string]).isRequired,
  spydrContentView:PropTypes.bool,
  tourConfig: PropTypes.object,
  onHelperButtonClicked: PropTypes.func,
};

export default Page;
