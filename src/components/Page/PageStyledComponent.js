import styled from "styled-components";
import img from "./mybg.png";

const css = `.container,
.tutorContainer,
.fontcontainer,
.relative,
.materialFontPosition,
.fonting,
.title {
  position: relative;
}

.container,
.pageWrapper {
  font-size: 15px;
  display: flex;
  flex-direction: column;
}

.header {
  font-size: 24px;
  color: #4c4f53;
  height: 45px;
  width: 100%;
}

.pageWrapper {
  padding: 15px;
  background: url(${img});
  box-shadow: inset rgba(6, 6, 6, 0.58) 0px 1px 6px, rgba(6, 6, 6, 0.48) 0px 1px 4px;
  min-height: calc(100vh - 81px);
  position: relative;
  height: auto;
  margin-bottom: auto;
}

.icon {
  margin-right: 5px;
}

.pageContent {
  background-color: white;
  flex: 1;
  padding: 24px;
}

.rzrpageContent {
  background-color: transparent;
  flex: 1;
  padding: 0px 14px;
}

.footer {
  background-color: #474544;
  color: #E4E4E4;
  padding-top: 5px;
  padding-left: 15px;
  padding-bottom: 5px;
}

.fonting,
.fonting1 {
  margin: 0px 38px;
  font-size: 13px;
  font-weight: 300;
}

.fonting1 {
  margin: 0px 35px;
  margin-bottom: 14px;
}

.tutorContainer {
  background: #3a3633;
  top: 0;
  height: 0;
}

.materialFontPosition {
  margin-left: 0px;
  margin-right: 3px;
}

.rzrFontPosition {
  margin-left: 10px;
  margin-right: 3px;
}

.relative {
  display: inline-block;
  width: 62%;
}

.header div,.header {
  display: inline-flex;
  align-items: center;
  align-content: center;
  justify-content: flex-start;

.none {
  display: none;
}
`;
const ScopedCSS = styled.div([css]);
export default ScopedCSS;
