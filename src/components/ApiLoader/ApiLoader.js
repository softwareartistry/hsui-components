import React, {Component} from "react";
import PropTypes from "prop-types";
import {CircularProgress} from "material-ui";
import {Container} from "./ApiLoaderStyledComponent";

class ApiLoader extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      loading: true,
      jsonData: {}
    };
    this.loadDataFromApiName();
  }

  loadDataFromApiName() {
    window.setTimeout(() => {
      let apiFun = window.reactBridge.apis[this.props.apiName];
      if (typeof apiFun === "function") {
        apiFun(this.props.requestObj).then((jsonData) => {
          this.setState({
            jsonData: jsonData,
            loading: false
          });
        });
      } else {
        this.setState({
          loading: false,
          jsonData: {
            error: `Unable to find API: ${this.props.apiName} Hint : Add ApiName in entry.js`
          }
        });
      }
    }, 0);
  }

  componentWillReceiveProps() {
    this.setState({
      loading: true
    });
    this.loadDataFromApiName();
  }

  render() {
    if (this.state.loading === true) {
      return <Container>
        <h1>{this.props.apiName}</h1>
        <CircularProgress />
      </Container>;
    }
    return (
      <Container>
        <h1>{this.props.apiName}</h1>
        {this.state.jsonData}
      </Container>
    );
  }
}
ApiLoader.defaultProps = {
  apiName: null,
  requestObj: {}
};
ApiLoader.propTypes = {
  apiName: PropTypes.string.isRequired,
  requestObj: PropTypes.object
};
export default ApiLoader;
