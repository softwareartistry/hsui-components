import React, {Component} from "react";
import PropTypes from "prop-types";
import {Container, IconContainer} from "./HelperButtonStyledComponent";
import {IconButton, FontIcon} from "material-ui";

const cssstyles={
  largeIcon: {
    width: 25,
    height: 25,
  },
  large: {
    width: 35,
    height: 35,
    padding: 0,
  },
};
class HelperButton extends Component {
  constructor(props, context) {
    super(props, context);
    this.helperButtonClicked = this.helperButtonClicked.bind(this);
    this.state = {
      name: "HelperButton"
    };
  }
  helperButtonClicked(evt) {
    this.props.onClick(evt);
  }

  render() {
    return (
      <Container>
        <IconContainer>
          <IconButton
            iconStyle={cssstyles.largeIcon}
            style={cssstyles.large}
            onClick={this.helperButtonClicked}
            disabled={this.props.disabled}
          >
            <FontIcon className="material-icons" color="#3a3633">help_outline</FontIcon>
          </IconButton>
        </IconContainer>
      </Container>
    );
  }
}

HelperButton.defaultProps = {};
HelperButton.propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool
};

export default HelperButton;
