import styled from "styled-components";

const Container = styled.div`
  position: relative;
`;

const IconContainer = styled.div`
  position: relative;
  &:hover: {
    cursor: pointer;
  }
`;

export {
  Container,
  IconContainer
}
